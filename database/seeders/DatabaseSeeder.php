<?php

namespace Database\Seeders;

use App\Models\ApiPost;
use Illuminate\Database\Seeder;
use App\Models\Post;
use Database\Factories\ApiPostFactory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        Post::factory(100)->create();
        ApiPost::factory(100)->create();
    }
}
