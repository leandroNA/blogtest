<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'age' => 25,
            'email_verified_at' => now(),
            'password' => bcrypt('admin'),
            'remember_token' => Str::random(10),
        ])->assignRole('admin');
        User::create([
            'name' => 'publico',
            'email' => 'publico@gmail.com',
            'age' => 25,
            'email_verified_at' => now(),
            'password' => bcrypt('publico'),
            'remember_token' => Str::random(10),
        ])->assignRole('publisher');
        User::factory(10)->create();;
    }
}
