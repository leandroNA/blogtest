<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name' => 'admin']);
        $publisher = Role::create(['name' => 'publisher']);

        Permission::create(['name' => 'post.index'])->syncRoles([$admin,$publisher]);
        Permission::create(['name' => 'post.show'])->syncRoles([$admin,$publisher]);
        Permission::create(['name' => 'post.create'])->syncRoles([$admin,$publisher]);
        Permission::create(['name' => 'post.edit'])->syncRoles([$admin]);
        Permission::create(['name' => 'post.destroy'])->syncRoles([$admin]);  

    }
}
