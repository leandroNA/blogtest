<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ApiPostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->sentence(),
            'description' => $this->faker->text(1200),
            'email'=> User::all()->random()->email,
            'date'=> $this->faker->dateTimeBetween("-1000 day", now()),
        ];
    }
}
