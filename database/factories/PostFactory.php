<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->text(1200),
            'user_id' => User::all()->random()->id,
            'created_at'=>$this->faker->dateTimeBetween("-1000 day" , now()),
        ];
    }
}
