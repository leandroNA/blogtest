<?php

namespace App\Http\Controllers;

use App\Http\Requests\post\StorePostRequest;
use App\Http\Requests\post\UpdatePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;


class PostController extends Controller
{
    protected $paginationTheme = 'bootstrap';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:post.edit')->only('edit','update');
        $this->middleware('can:post.create')->only('create','store');
        $this->middleware('can:post.destroy')->only('destroy');
    }


    public function index()
    {
        
        $posts = Post::where('user_id',auth()->user()->id)->latest('created_at')->get();
        return view('post.index',compact('posts'));
    }
 
    public function create()
    {
        $post = new Post();
        return view('post.create',compact('post'));
    }

    public function store(StorePostRequest $request)
    {
        $request['user_id'] = auth()->user()->id;
        $post = Post::create($request->all());
        return redirect()->route('post.index');
    }

    public function show(Post $post)
    {
        return view('post.show',compact('post'));
    }

    public function edit(Post $post)
    {   
        if (!auth()->user()->hasRole('admin')) {
            $post = Post::where('id',$post->id)->where('user_id',auth()->user()->id)->first();
            if(!$post){
                return back();
            }
        }
        return view('post.edit',compact('post'));
    }

    public function update(UpdatePostRequest $request, Post $post)
    {
        $post->update($request->all());
        return redirect()->route('post.index');
    }

    public function destroy( Post $post)
    {
        $post->delete();
        if ($post) {
            return response()->json([
                'message' => 'Eliminado',
            ]);
        }else{
            return response()->json([
                'message' => 'Algo salió mal',
            ],501);
        }  
    }
}
