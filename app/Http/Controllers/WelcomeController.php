<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Post;

use Illuminate\Support\Carbon;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::whereDate('created_at','LIKE',$request->search.'%')->latest('created_at')->get();
        // dd($request->search,date('Y-m-d H:i:s',strtotime($request->search)));
        // dd($request->all(), $posts[1]);

        return view('welcome.index',compact('posts'));
    }


    public function show(Post $post)
    {
        // $this->validate($request,[
        //     'post' => 'required|numeric|exists,posts'
        // ]);
        // $post = Post::find($request->id);
        return view('welcome.show', compact('post'));
    }
}
