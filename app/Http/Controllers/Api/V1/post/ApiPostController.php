<?php

namespace App\Http\Controllers\Api\V1\post;

use App\Http\Controllers\Controller;
use App\Http\Requests\post\StorePostRequest;
use App\Http\Requests\post\UpdatePostRequest;
use App\Http\Resources\PostResource;
use App\Models\ApiPost;
use App\Models\Post;
use Illuminate\Http\Request;

class ApiPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PostResource::collection(Post::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        return (new PostResource(Post::create($request->all())))
        ->additional(['msg' => 'Post Creado con exito'])
        ->response()
        ->setStatusCode(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ApiPost  $apiPost
     * @return \Illuminate\Http\Response
     */
    public function show(Post $apipost)
    {
        // dd($post);
        return response()->json([
            'res' => true,
            'msg' => $apipost
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ApiPost  $apiPost
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $apipost)
    {
        $apipost->update($request->all());
        return response()->json([
            'res' => true,
            'msg' => 'Actualizado con exito'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ApiPost  $apiPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $apipost)
    {
        $apipost->delete();
        if($apipost){
            return response()->json([
                'res' => true,
                'msg' => 'Post Eliminado'
            ],200);
        }else{
            return response()->json([
                'msg' => 'Algo salió mal',
            ],501);
        }
       
    }
}
