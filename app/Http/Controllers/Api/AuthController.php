<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function register(Request $request){
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'age' => ['required', 'numeric', 'min:18', 'max:110'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user =  User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'age' => $request['age'],
            'password' => Hash::make($request['password']),
        ])->assignRole('publisher');;

        return response()->json([
            'res' => true,
            'msg' => 'Usuario registrado con exito'
        ],200   );

    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);

        $user = User::with('roles')->where('email', $request->email)->first();
 
        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'msg' => ['Credenciales Incorrectas.'],
            ]);
        }
    
        $token = $user->createToken($request->email)->plainTextToken;
        return response()->json([
            'res' => true,
            "token" => $token,
            "user" => $user,
        ],200);
    }

    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'res' => true,
            'msg' => 'Sesion finalizda'
        ],200);    
    }
}
