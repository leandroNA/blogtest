<?php

namespace App\Http\Requests\post;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $post = $this->route()->parameter('post');
        return [
            'title' => "required|string|unique:posts,title,".$post->id,
            'description' => 'required|string',
            'user_id' => 'required|exists:users,id'
        ];
    }
}
