<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        // helpers
        return [
            'id' => $this->id,
            'title' => Str::upper($this->title),
            'description' => $this->description,
            'created_at' => $this->created_at->format('d-m-Y'),
            'user_id' => $this->user_id,
        ];
    }

    public function with($request)
    {
        return [
            'res' => true,
        ];
    }
}
