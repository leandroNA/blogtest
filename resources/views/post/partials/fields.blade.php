
  <div class="form-group">
    <label for="Input">Titulo</label>
    <input type="text" class="form-control" id="title" name="title" placeholder="Digite el titulo" value="{{old('title') ?? $post->title ?? ''}}">
  </div>
  @error('title')
    <span class="text-danger">{{$message}}</span>
  @enderror
  <div class="form-group">
    <label for="Textarea">Descripcion</label>
    <textarea style="height: 400px" class="form-control w-full" id="description" name="description" rows="3" placeholder="Escriba su texto">{{old('description') ?? $post->description ?? ''}}</textarea>
  </div>
  <input type="hidden" name="user_id" value="{{old('user_id') ?? $post->user->id ?? ''}}" >
  @error('user_id')
    <span class="text-danger">{{$message}}</span>
  @enderror
  
