@extends('layouts.app')

@section('content')
<div class="container">
    <div class="float-right">
        <a class="btn btn-primary" href="{{ route('post.create')}}" role="button">Agregar Publicación <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
            </svg>
        </a>
    </div>
    <hr>
    <div class="row justify-content-center">
    <!-- @foreach($posts as $post)
        <div class="col-md-6 p-2">
            <div class="card shadow" style="width: auto;">
                <div class="card-body">
                    <h5 class="card-title"><b>{{ $post->title}}</b></h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ date('d-m-Y',strtotime($post->created_at)) }}</h6>
                    <p class="card-text">{{ substr($post->description,0,200) }}</p>
                    <a href="{{ route('post.show', $post) }}" class="btn btn-outline-primary btn-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                            <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                        </svg>
                        Ver
                    </a>
                    @can('post.edit')
                    <a href="{{ route('post.edit', $post) }}" class="m-2 btn btn-outline-success btn-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                        </svg>
                        Editar
                    </a>
                    @endcan
                    @can('post.destroy')
                    <form action="{{route('post.destroy', $post) }}" method="POST" class="btn m-0 p-0">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-outline-danger">Eliminar</button>
                    </form>
                    @endcan
                </div>
            </div>
        </div>
        @endforeach -->
        <table id="table_id" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Titulo</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->title }}</td>
                        <td>
                            <a href="{{ route('post.show', $post) }}" class="btn btn-outline-primary btn-sm">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                    <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                </svg>
                                Ver
                            </a>
                            @can('post.edit')
                            <a href="{{ route('post.edit', $post) }}" class="m-2 btn btn-outline-success btn-sm">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                </svg>
                                Editar
                            </a>
                            @endcan
                            @can('post.destroy')
                            <form action="{{route('post.destroy', $post) }}" method="POST" class="btn m-0 p-0" onclick="eliminar(this)">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-outline-danger">Eliminar</button>
                            </form>
                            @endcan    
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table> 
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            order:false,
             language: {
                    "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
    } );

    function eliminar(id){
    event.preventDefault();
    ruta = window.location;
    action = event.srcElement.form.action
    Swal.fire({
        title: 'Estas seguro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#e65100',
        cancelButtonColor: 'green',
        // timer : 3000,
    }).then((result) => {
        if (result.value) {
            token = $("#token").val()
            console.log(token)
            $.ajax({
                url: action,
                headers: {'X-CSRF-TOKEN' : '{{ csrf_token() }}'},
                type: 'DELETE',
                success: function(result) {
                    if (result) {
                        Swal.fire(
                            'Eliminado!',
                            result.message,
                            'success',
                        )
                        setTimeout(() => {
                            location.reload()
                        }, 1000);
                        
                    }
                },
                error: function (xhr) {
                    Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    timer:1000,
                    text: xhr.responseJSON.message,
                    })
                }
            })
        }
        return false;
        
        })
    }
</script>


@endsection