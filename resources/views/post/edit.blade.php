@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12  p-2">
            <div class="card shadow" style="width: auto;">
                <div class="card-body">
                    <h5 class="card-title"><b>Editar Publicacion</b></h5>
                    <div class="container">
                        <form method="POST" action="{{ route('post.update', $post) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            @include('post.partials.fields')
                            <br>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    

@endsection