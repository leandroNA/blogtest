@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 p-2">
            <div class="card shadow" style="width: auto;">
                <div class="card-body">
                    <h5 class="card-title"><b>{{ $post->title}}</b></h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ date('d-m-Y',strtotime($post->created_at)) }}</h6>
                    <p class="card-text">{{ $post->description }}</p>
                    <a href="{{ url()->previous() }}" class="card-link">Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')



@endsection