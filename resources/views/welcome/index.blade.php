@extends('layouts.app')

@section('content')
<div class="container">
      <form href="{{ route('welcome.index') }}" class="d-flex col-3 " role="search">
        <input name="search" class="form-control me-2" type="date" placeholder="Search" aria-label="Search" value="{{ Request::get('search')}}">
        <button class="btn btn-outline-success" type="submit">Buscador</button>
      </form>
    <div class="row justify-content-center">
    @foreach($posts as $post)
        <div class="col-md-6 p-2">
            <div class="card shadow" style="width: auto;">
                <div class="card-body">
                    <h5 class="card-title"><b>{{ $post->title}}</b></h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ date('d-m-Y',strtotime($post->created_at)) }}</h6>
                    <p class="card-text">{{ substr($post->description,0,200) }}</p>
                    
                    <a href="{{ route('welcome.show',$post) }}" class="card-link">Ver mas...</a>
                    @can('post.edit')
                    <a href="{{ route('post.edit', $post) }}" class="m-2 btn btn-outline-success btn-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                        </svg>
                        Editar
                    </a>
                    @endcan
                    @can('post.destroy')
                    <form action="{{route('post.destroy', $post) }}" method="POST" class="btn m-0 p-0">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-outline-danger">Eliminar</button>
                    </form>
                    @endcan
                    <!-- <a href="#" class="card-link">Another link</a> -->
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


@endsection
