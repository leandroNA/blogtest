<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// publica
Route::get('/', [WelcomeController::class, 'index'])->name('welcome.index');
Route::get('welcome/{post}', [WelcomeController::class, 'show'])->name('welcome.show');

// protegidas
Auth::routes();

Route::resource('post', PostController::class);
